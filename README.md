# GDScript / Godot Color Theme

This is a simple color theme to make VSCode and the syntax highlighting look like [Godot](https://godotengine.org/)'s internal editor.  
To make this theme work and for additional GDScript specific functionality, like e.g. code completion, I recommend the [godot-tools](https://marketplace.visualstudio.com/items?itemName=geequlim.godot-tools) extension.

## Use for `.gd` Files Only
To be able to apply this theme to `.gd` files only, I recommend the [Theme by language](https://marketplace.visualstudio.com/items?itemName=jsaulou.theme-by-language) extension.  
Unfortunately, VSCode does not have this functionality built in as of now.

## Font
To my knowledge, a simple color theme without JavaScript can't change fonts.
You'll have to install it separately on your system and then apply it through user settings.  
It is called [Hack and you can find it here](https://github.com/source-foundry/Hack).  
After installing the font, paste this into your `settings.json` file:
```
"[gdscript]": {
    "editor.fontFamily": "Hack, Consolas, 'Courier New', monospace"
}
```

## Innacuracies
Although I tried to make the theme truthful to the original by taking the color codes directly from Godot, it wasn't entirely possible.   
The syntax highlighter of the [godot-tools](https://marketplace.visualstudio.com/items?itemName=geequlim.godot-tools) extension does not make some of the distinctions the internal editor does.   
To fix this, it would need to be changed in the TextMate grammar of the syntax highlighter of that extension.

Known inconsistencies:
* In the internal editor, the names of functions have different colors in the definition and when they're called, whereas here they don't.   
(Decided to use the color of a function call for both.)
* Stuff like `true`, `false` and `null` is treated as keywords in the internal editor, whereas here they are treated as constants.  
(Decided to make constants, like `Input.MOUSE_MODE_HIDDEN`, have the same color as key words rather than the other way around.)
* The `$SomeNode` shorthand for selecting a node is not recognized as a node path.

If you find more, you're welcome to leave me a note or open a merge request on the theme's [GitLab repo](https://gitlab.com/JamesSauer/GDScript-Theme-for-VSCode/issues)!




